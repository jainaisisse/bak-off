#!/usr/bin/env bash

PS3='What would you like to do ? '
options=("Create offers on the kama exchange platform" "Buy subscriptions" "Close character pages" "Quit")
select opt in "${options[@]}"
do
    case ${opt} in
        "Create offers on the kama exchange platform")
            npm start -- --offers
            break
            ;;
        "Buy subscriptions")
            npm start -- --subscriptions
            break
            ;;
        "Close character pages")
            npm start -- --pages
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "Invalid option.";;
    esac
done
