/*!
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 bakoff
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

import puppeteer from "puppeteer";
import * as constants from "../constants";

import { getLogger } from "log4js";
const logger = getLogger("Browser");

/**
 * Abstraction layer around the requests made on the Dofus website.
 */
class Browser {
    /**
     * A path to a Chromium bundle. If null, the built-in Chromium bundle will be used.
     * @type {String}
     */
    chromiumPath = null;

    /**
     * Whether to disable sandbox when running Chromium.
     * @type {Boolean}
     */
    disableSandbox = false;

    /**
     * The account used to authenticate.
     * @type {Account}
     * @private
     */
    account = null;

    /**
     * The browser instance.
     * @type {null}
     */
    browser = null;

    /**
     * The browser page.
     * @type {null}
     */
    page = null;

    /**
     * Constructs a browser object.
     * @param {String} [chromiumPath=null] A path to an existing Chromium executable.
     * @param {Boolean} [disableSandbox=false] Whether to disable sandbox when running Chromium.
     */
    constructor(chromiumPath = null, disableSandbox = false) {
        this.chromiumPath = chromiumPath;
        this.disableSandbox = disableSandbox;
    }

    /**
     * Initializes this browser.
     */
    async initialize() {
        if (this.browser === null) {
            this.browser = await puppeteer.launch({
                executablePath: this.chromiumPath !== null ? this.chromiumPath : undefined,
                headless: process.env.HEADLESS !== "0",
                args: this.disableSandbox ? ["--no-sandbox", "--disable-setuid-sandbox"] : undefined
            });
        }
        if (this.page === null) {
            this.page = await this.browser.newPage();
        }
    }

    /**
     * Shuts this browser down.
     */
    async shutdown() {
        if (this.browser !== null) {
            await this.browser.close();
            this.browser = null;
        }
    }

    /**
     * Sets a new account for this browser. Clears all the previous loaded accounts.
     * @param {Account} account The new account.
     */
    async setAccount(account) {
        this.account = account;
    }

    /**
     * Authenticates this account on the website by logging in.
     */
    async login() {
        this.log(`Connecting on '${this.account.username}'...`);

        // Load the home page before logging in to make sure we won't hit the prehome after login.
        await this.page.goto(constants.DOFUS_URL, { waitUntil: "domcontentloaded" });
        logger.debug("Home page loaded.");

        // Load login page.
        await this.page.goto(constants.DOFUS_LOGIN_URL);
        logger.debug("Login page loaded.");

        // Log in.
        await this.sendLoginRequest();

        // Check if we hit the TOU.
        if (this.page.url().search(/(tou|cgu)/g) !== -1) {
            // We hit them. Validate !
            this.log("We hit the TOU. Validating...");
            await this.validateTOU();
        }

        // Check if login failed.
        const connectLinks = await this.page.$(".ak-connect-links");
        if (connectLinks !== null) {
            // Login failed.
            throw new Error("Login failed. Check your credentials.");
        }

        // Check for subscription.
        await this.checkSubscription();

        this.log(`Connected on '${this.account.username}' `
            + `(${this.account.getSubscriptionString()}).`);
    }

    /**
     * Logs out the currently connected account.
     */
    async logout() {
        this.log(`Logging out '${this.account.username}'...`);

        if (await this.page.$(".ak-logged-account") === null) {
            // No account connected.
            return;
        }
        await this.page.goto(constants.DOFUS_LOGOUT_URL);
        if (await this.page.$(".ak-logged-account") !== null) {
            // Disconnection failed.
            throw new Error("Error while disconnecting account.");
        }
    }

    /**
     * Creates an offer with the specified Ogrines amount and the specified kamas rate.
     * @param {number} ogrinesAmount The amount of Ogrines we want to get.
     * @param {number} kamasRate The kamas rate.
     * @param {string|null} [server=null] The server on which the offer will be created, if none
     * given, the default server will be used.
     */
    async createOffer(ogrinesAmount, kamasRate, server = null) {
        // Load the BaK.
        await this.page.goto(constants.BAK_URL);

        // If we landed on the maintenance page, the account can't use the kama exchange platform.
        if (this.page.url().search("maintenance") !== -1) {
            throw new Error("This account can't use the kama exchange platform.");
        }

        // If we landed on the server selection page, select the right server.
        if (this.page.url().search("selection") !== -1) {
            this.log("Server selection needed, selecting server...");
            await this.selectServer(server);
            this.log("Server selected.");
        }

        const serverName = await this.page.evaluate(() => {
            return document.querySelector("select[name='dofus-server'] option[selected=true]")
                .innerText;
        });
        this.log(`'${serverName}' is the selected server.`);

        // Select "Create offer" tab.
        await this.page.click(`a[href='${constants.BAK_NAVIGATION_CREATE_OFFER_URL}']`);
        await this.page.waitForSelector("li.on "
            + `a[href='${constants.BAK_NAVIGATION_CREATE_OFFER_URL}']`);

        // Select "Offering kamas" tab.
        await this.page.click(`a[href='${constants.BAK_NAVIGATION_OFFER_KAMAS_URL}']`);
        await this.page.waitForSelector(
            `a.ak-on[href='${constants.BAK_NAVIGATION_OFFER_KAMAS_URL}']`);

        // Check if we already have an ongoing offer.
        const ongoingOffer = await this.page.$("input[name='cancel_id']");
        if (ongoingOffer !== null) {
            throw new Error("You already have an ongoing offer.");
        }

        // Fill in form.
        await this.page.evaluate(() => {
            document.querySelector("input[name='give']").value = "";
            document.querySelector("input[name='want']").value = "";
            document.querySelector("input[name='rate']").value = "";
        });
        await this.page.type("input[name='give']", (ogrinesAmount * kamasRate).toString(10));
        await this.page.type("input[name='want']", ogrinesAmount.toString(10));
        await this.page.type("input[name='rate']", kamasRate.toString(10));

        // Check if the request is valid.
        const disabledButton = await this.page.$(
            ".ak-block-offers form input.disabled[type=submit]");
        if (disabledButton !== null) {
            throw new Error("Cannot create a new offer, submit button is disabled, check rates.");
        }

        // We're all set, time to fire the request !
        this.log(`Creating offer for ${ogrinesAmount} Ogrines at ${kamasRate} kamas/Ogrine.`);
        await this.page.click(".ak-block-offers form input[type=submit]");
        await this.page.waitForSelector(".ak-bak-confirmations");

        // Check the values.
        const giveValue = await this.page.evaluate(() => {
            return parseInt(document.querySelector("input[name='give']").value, 10);
        });
        const wantValue = await this.page.evaluate(() => {
            return parseInt(document.querySelector("input[name='want']").value, 10);
        });
        const rateValue = await this.page.evaluate(() => {
            return parseInt(document.querySelector("input[name='rate']").value, 10);
        });
        if (giveValue !== ogrinesAmount * kamasRate || wantValue !== ogrinesAmount
            || rateValue !== kamasRate) {
            throw new Error("Rates aren't matching the given rate in the configuration," +
                "upon offer creation confirmation, aborting.");
        }

        // Values are looking good, confirm offer.
        this.log("Confirming offer creation.");
        await this.page.click("form .ak-bak-confirmations input[type=submit]");
        await this.page.waitForNavigation();

        // Checking if offer was successfully created.
        const successImage = await this.page.$(".ak-bak-confirmations img.ak-img-congrat");
        if (successImage === null) {
            throw new Error("Error while confirming offer creation, try again later.");
        }

        // If we get here, all requests succeeded.
        this.account.operationSucceeded = true;
        this.log("✓ Successfully created offer.");
    }

    /**
     * Closes the character pages on this account.
     * This hides all the informations about the characters on their pages by modifying the pages
     * options.
     */
    async closeCharacterPages() {
        // Load the page once to get data.
        await this.page.goto(constants.DOFUS_URL);
        logger.debug("Home page loaded.");

        // Get characters list.
        const characters = await this.page.evaluate(() => {
            const charactersList = {};
            const characterElements = document.querySelectorAll(
                ".ak-characters-list .ak-character .ak-name a");
            [].forEach.call(characterElements, (element) => {
                charactersList[element.innerText.trim()] = element.href;
            });
            return charactersList;
        });
        logger.debug(characters);
        // Close each character's page.
        for (const character in characters) {
            if (!characters.hasOwnProperty(character)) { continue; }
            const pageUrl = characters[character];
            this.log(`Closing page for '${character}'...`);
            await this.closeCharacterPage(pageUrl);
        }
        // If we get here, all requests succeeded.
        this.account.operationSucceeded = true;
        this.log("✓ Successfully closed character pages.");
    }

    /**
     * Subscribes the account with Ogrines, using the given article ID.
     * @param {string} [lang=en] The language to use.
     */
    async subscribe(lang = "en") {
        const shopPaymentUrl = constants.SHOP_PAYMENT_URLS[lang];
        const articleId = constants.SHOP_PAYMENT_ARTICLES[lang];
        if (typeof shopPaymentUrl === "undefined" || typeof articleId === "undefined") {
            throw new Error(`Language '${lang}' isn't supported for subscribing.`);
        }
        this.log("Subscribing...");

        await this.page.goto(shopPaymentUrl);
        logger.debug("Page changed.");

        // Get subscription pack URL.
        const packUrl = await this.page.evaluate((packArticleId) => {
            const packElements = document.querySelectorAll(
                ".ak-catalog-article-list .ak-mosaic-item-article");
            const packElement = [].find.call(packElements, (element) => {
                return element.querySelector("a").href.indexOf(`/${packArticleId}-`) !== -1;
            });
            return packElement !== null ? packElement.querySelector("a").href : null;
        }, articleId);

        if (packUrl === null) {
            throw new Error(`Cannot find subscription pack with ID '${articleId}'.`);
        }

        // Go to article.
        await this.page.goto(packUrl);
        logger.debug("Page changed.");

        // Click on "Buy in Ogrines".
        try {
            await this.page.click(".js-buyinogr");
        } catch (error) {
            throw new Error("Buying subscription in Ogrines isn't available.");
        }
        try {
            await this.page.waitForSelector(
                "form.ak-tooltip-process input[name='payment'][value='OG']",
                { timeout: 5000 });
        } catch (error) {
            throw new Error("Failed to subscribe account, check your Ogrines amount and language.");
        }

        // Click on "Confirm" in tooltip.
        await this.page.click("form.ak-tooltip-process input[type=submit]");
        try {
            await this.page.waitForSelector(".ak-tooltip-process span.ak-success");
        } catch (error) {
            throw new Error("Failed to subscribe account, " +
                "but it may have succeeded still, check manually.");
        }

        // If we get here, all requests succeeded.
        this.account.operationSucceeded = true;
        this.log("✓ Successfully subscribed account.");
    }

    /**
     * Sends a login request.
     */
    async sendLoginRequest() {
        await this.page.type("#userlogin", this.account.username);
        await this.page.type("#userpass", this.account.password);
        await this.page.$eval("form[name='connectform']", form => form.submit());
        logger.debug("Login request sent.");
        await this.page.waitForSelector(".ak-logged-account");
        logger.debug("Logged in.");
    }

    /**
     * Closes a characters's page.
     * @param {string} pageUrl The character's page URL.
     * @private
     */
    async closeCharacterPage(pageUrl) {
        await this.page.goto(pageUrl + constants.CHARACTER_PAGES_MANAGE_SUFFIX);
        logger.debug("Page changed.");
        if (await this.page.$("input[name='show_alignment'][checked='checked']") !== null) {
            await this.page.click("input[name='show_alignment']");
        }
        if (await this.page.$("input[name='show_ladder'][checked='checked']") !== null) {
            await this.page.click("input[name='show_ladder']");
        }
        if (await this.page.$("input[name='show_lifestream'][checked='checked']") !== null) {
            await this.page.click("input[name='show_lifestream']");
        }
        if (await this.page.$("input[name='show_spells_equipments'][checked='checked']") !== null) {
            await this.page.click("input[name='show_spells_equipments']");
        }
        if (await this.page.$("input[name='show_caracteristics'][checked='checked']") !== null) {
            await this.page.click("input[name='show_caracteristics']");
        }
        if (await this.page.$("input[name='show_success'][checked='checked']") !== null) {
            await this.page.click("input[name='show_success']");
        }
        if (await this.page.$("input[name='hide_jobs'][checked='checked']") === null) {
            await this.page.click("input[name='hide_jobs']");
        }
        await this.page.click("#ak-character-admin-form input[type=submit]");
        logger.debug("Submitting character page edition.");
        await this.page.waitForNavigation();
        logger.debug("Page changed.");
    }

    /**
     * Validates the TOU.
     * @private
     */
    async validateTOU() {
        await this.page.waitForSelector("input[name='validate_cgu']");
        await this.page.click("input[name='validate_cgu']");
        await this.page.click("main.main form input[type=submit]");
        logger.debug("TOU validated.");
        await this.page.waitForNavigation();
        logger.debug("Page changed.");
    }

    /**
     * Selects a server from the available servers on the page. If no server ID is given, selects
     * the first server available.
     * @param {string|null} [server=null] The server that needs to be selected.
     * @private
     */
    async selectServer(server = null) {
        let selectionUrl;
        if (server !== null) {
            // A server name was given, search the corresponding server block.
            selectionUrl = await this.page.evaluate((chosenServer) => {
                const block = [].find.call(document.querySelectorAll(".ak-block-server"),
                        elem => elem.querySelector(".ak-name").innerText === chosenServer);
                return block !== null ? block.getAttribute("href") : null;
            }, server);
        } else {
            // No server given, take the first one.
            selectionUrl = await this.page.evaluate(() => {
                const block = document.querySelector(".ak-block-server");
                return block !== null ? block.getAttribute("href") : null;
            });
        }
        // If server URL is null, the server wasn't found or no servers are available.
        if (selectionUrl === null) {
            throw new Error("Unable to select server : server not found or no servers available.");
        }
        await this.page.click(`a[href='${selectionUrl}']`);
        await this.page.waitForSelector("select[name='dofus-server']");
    }

    /**
     * Checks for the account's subscription and sets the appropriate values in the account.
     * @private
     */
    async checkSubscription() {
        const subscribe = await this.page.$(".ak-game-subscribe .ak-infos-small");
        const notSubscribe = await this.page.$(".ak-game-not-subscribe");
        if (notSubscribe !== null) {
            // We're not a subscriber.
            this.account.isSubscriber = false;
        } else if (subscribe !== null) {
            // We're a subscriber.
            const endDate = await this.page.evaluate(el => el.innerHTML.split(" ").pop(),
                subscribe);
            this.account.isSubscriber = true;
            this.account.subscriptionEndDate = endDate;
        } else {
            // WTF are we ?
            throw new Error("Error while checking account subscription.");
        }
    }

    /**
     * Logs a given message with a given level, prepending it with the current account.
     * @param {string} message The message.
     * @param {string} [level=info] The level.
     * @private
     */
    log(message, level = "info") {
        message = `[${this.account.username}] ${message}`;
        logger.log(level, message);
    }
}

export default Browser;
