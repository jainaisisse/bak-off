/*!
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 bakoff
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

import { configure } from "log4js";

/**
 * The log level used to display all logs, including traces.
 * @type {String}
 */
const TRACE_LOG_LEVEL = "TRACE";
/**
 * The log level used while debugging.
 * @type {String}
 */
const DEBUG_LOG_LEVEL = "DEBUG";
/**
 * The log level used while in production.
 * @type {String}
 */
const PRODUCTION_LOG_LEVEL = "INFO";
/**
 * The default log level, according to the NODE_ENV variable.
 * @type {String}
 */
const DEFAULT_LOG_LEVEL = (() => {
    switch (process.env.NODE_ENV) {
        case "development-trace":
            return TRACE_LOG_LEVEL;
        case "development":
            return DEBUG_LOG_LEVEL;
        default:
            return PRODUCTION_LOG_LEVEL;
    }
})();
/**
 * The log pattern used while debugging.
 * @type {String}
 */
const DEBUG_PATTERN = "%[[%d{ABSOLUTE}][%-5p][%c]%] %m";
/**
 * The log pattern used while in production.
 * @type {String}
 */
const PRODUCTION_PATTERN = "%[[%d{ABSOLUTE}][%-5p]%] %m";
/**
 * The default log pattern, according to the NODE_ENV variable.
 * @type {String}
 */
const DEFAULT_PATTERN = process.env.NODE_ENV && process.env.NODE_ENV.startsWith("development")
    ? DEBUG_PATTERN : PRODUCTION_PATTERN;

/**
 * Configures log4js, adding the appenders and setting log level from environment.
 * @param {String|undefined} [pattern] The log pattern to use.
 * @param {String|undefined} [logLevel] The maximum log level to print.
 * @returns {void}
 */
const configureLogger = (pattern, logLevel) => {
    pattern = pattern || DEFAULT_PATTERN;
    logLevel = logLevel || DEFAULT_LOG_LEVEL;

    configure({
        appenders: [{
            type: "logLevelFilter",
            level: logLevel,
            appender: {
                type: "console",
                layout: {
                    type: "pattern",
                    pattern
                }
            }
        }]
    });
};

/**
 * Enables debug log. From now on, all debug logs will be printed.
 * @returns {void}
 */
const enableDebugLog = () => {
    configureLogger(DEBUG_PATTERN, DEBUG_LOG_LEVEL);
};

/**
 * Disables debug log. From now on, only production logs will be printed.
 * @returns {void}
 */
const disableDebugLog = () => {
    configureLogger(PRODUCTION_PATTERN, PRODUCTION_LOG_LEVEL);
};

export { configureLogger, enableDebugLog, disableDebugLog };
