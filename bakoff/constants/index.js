/*!
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 bakoff
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

// File paths.
export const CONFIG_PATH = "./config/config.json";
export const ACCOUNTS_PATH = "./config/accounts.txt";
export const ACCOUNTS_FAILED_PATH = "./config/accounts.failed.txt";

// Dofus website URLs.
export const DOFUS_URL = "https://www.dofus.com";
export const DOFUS_LOGIN_URL = "https://www.dofus.com/fr/identification";
export const DOFUS_LOGOUT_URL = `https://account.ankama.com/sso?action=logout&from=${DOFUS_URL}`;
export const DOFUS_SECURE_URL = "https://secure.dofus.com";

export const BAK_URL = `${DOFUS_URL}/fr/achat-bourses-kamas-ogrines`;
export const BAK_NAVIGATION_CREATE_OFFER_URL =
    "/fr/achat-bourses-kamas-ogrines/achat-kamas/creer-offre";
export const BAK_NAVIGATION_OFFER_KAMAS_URL =
    "/fr/achat-bourses-kamas-ogrines/achat-ogrines/creer-offre";

export const CHARACTER_PAGES_MANAGE_SUFFIX = "/gestion-profil";

export const SHOP_PAYMENT_URLS = {
    en: `${DOFUS_SECURE_URL}/en/shop/18-subscriptions`,
    fr: `${DOFUS_SECURE_URL}/fr/boutique/18-abonnements`,
    es: `${DOFUS_SECURE_URL}/es/tienda/18-abonos`,
    de: `${DOFUS_SECURE_URL}/de/shop/18-abonnements`,
    it: `${DOFUS_SECURE_URL}/it/shop/18-abbonamenti`,
    pt: `${DOFUS_SECURE_URL}/pt/loja/18-assinaturas`
};
export const SHOP_PAYMENT_ARTICLES = {
    en: 10214,
    fr: 10214,
    es: 10215,
    de: 10214,
    it: 10214,
    pt: 10215
};
