/*!
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 bakoff
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

import ES6Error from "es6-error";

/**
 * An exception thrown whenever an error happens while handling configuration files.
 * @extends ES6Error
 */
class ConfigurationError extends ES6Error {
    /**
     * Constructs the error.
     * @param {string} message The message.
     */
    constructor(message) {
        super(message);
    }
}

import { readJson } from "fs-promise";

/**
 * Handles configuration files.
 */
class Configuration {
    /**
     * The name of the file currently loaded.
     * @type {?string}
     */
    fileName = null;
    /**
     * The configuration object loaded from the file.
     * @type {Object}
     */
    configuration = null;

    /**
     * Constructs a configuration object.
     * @param {string} fileName The configuration file to read.
     */
    constructor(fileName) {
        if (!fileName || fileName === null) {
            throw new ConfigurationError("Configuration file name cannot be empty.");
        }
        this.fileName = fileName;
    }

    /**
     * Loads the configuration.
     * @returns Promise
     */
    async load() {
        this.configuration = await readJson(this.fileName);
    }

    /**
     * Returns the value associated to the given key from the configuration.
     * @param {string} key The configuration key.
     * @param {*} [defaultValue=undefined] The default value if the key is not found. If no default
     * value is given and the key doesn't exist, an exception is thrown.
     * @returns {*} The value associated to the given key or the default value.
     */
    get(key, defaultValue = undefined) {
        if (!this.configuration.hasOwnProperty(key)) {
            if (defaultValue === undefined) {
                throw new Error(`Configuration for '${key}' is missing.`);
            }
            return defaultValue;
        }
        return this.configuration[key];
    }
}

export default Configuration;
